package com.bigo.web.fastspring.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
public class FileUtil {

	public static boolean createDir(String dirName){
		File dir = new File(dirName);
		if(dir.exists()) {
			log.error("创建目录" + dirName + "失败，目标目录已存在");
			return false;
		}

		//避免windows和linux的文件目录分隔符不一样
		if(dirName.endsWith(File.separator)) {
			dirName = dirName + File.separator;
		}

		//创建目录
		if(dir.mkdirs()) {
			log.info("创建目录" + dirName + "成功！");
			return true;
		}else {
			log.error("创建目录" + dirName + "失败！");
			return false;
		}
	}


}
