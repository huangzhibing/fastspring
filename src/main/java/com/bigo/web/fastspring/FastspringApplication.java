package com.bigo.web.fastspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(FastspringApplication.class, args);
	}

}
