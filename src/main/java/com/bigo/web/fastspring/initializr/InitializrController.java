package com.bigo.web.fastspring.initializr;

import com.bigo.web.fastspring.util.FileUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class InitializrController {

	private static Configuration getConfiguration(String templateDirectory) {
		Configuration configuration = new Configuration(Configuration.VERSION_2_3_22);

		try {
			configuration.setTagSyntax(Configuration.AUTO_DETECT_TAG_SYNTAX);
			configuration.setDirectoryForTemplateLoading(new File(templateDirectory));
		} catch (IOException e) {
			log.error("loading template failed:", e);
			throw new RuntimeException(e);
		}
		return configuration;
	}

	public static void main(String[] args) {

		Map<String, Object> dataMap = new HashMap<String, Object>();

		String templateDirectory = "src/main/resources/template";

		String templateFile = "applicationTemplate.ftl";

		String groupId = "com.bigo.web.";

		String name = "fastspring";

		String applicationName = Character.toUpperCase(name.charAt(0)) + name.substring(1) + "Application" + ".java";

		String head = name + "/src/main/java/";

		String targetPath = "E:/test/"+ UUID.randomUUID() + "/" + head + groupId.replace(".","/") + name;

		dataMap.put("groupAndName", groupId+name);

		dataMap.put("name", name);

		Writer writer = null;
		if(FileUtil.createDir(targetPath)) {
			try {
				writer = new FileWriter(targetPath +"/"+applicationName);

				Template template = getConfiguration(templateDirectory).getTemplate(templateFile, "UTF-8");
				template.process(dataMap, writer);
			} catch (TemplateException e) {
				log.error("process template failed:", e);
				throw new RuntimeException(e);
			} catch (IOException e) {
				log.error("generate template failed:", e);
				throw new RuntimeException(e);
			} finally {
				try {
					writer.close();
				} catch (IOException e) {
					log.error("writer template failed:", e);
					throw new RuntimeException(e);
				}
			}
		}

	}
}
